@Grab(group='org.eclipse.jetty', module='jetty-server', version='9.4.16.v20190411')
import org.eclipse.jetty.server.Server
@Grab(group='org.eclipse.jetty', module='jetty-servlet', version='9.4.16.v20190411')
import org.eclipse.jetty.servlet.*
import groovy.servlet.*

//@Grab(group='org.eclipse.jetty.aggregate', module='jetty-all', version='7.6.15.v20140411')
class JettyServer {
    static Server server
    static void startJetty() {
        server = new Server(8282)

        def handler = new ServletContextHandler(ServletContextHandler.SESSIONS)
        handler.contextPath = '/'
        handler.resourceBase = '.'
        handler.welcomeFiles = ['boardA.html']
        handler.addServlet(GroovyServlet, '/scripts/*')
        def filesHolder = handler.addServlet(DefaultServlet, '/')
        filesHolder.setInitParameter('resourceBase', '../resource')
        server.handler = handler
        server.start()
    }

    static void stopJetty(){
        server.stop()
        server = null
    }

}

