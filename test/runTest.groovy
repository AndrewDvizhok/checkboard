/**
 * Welcome message
 */
println "Hello from test"

import junit.framework.Test
import junit.textui.TestRunner

/**
 * Test without server
 */
class AllTests {
    static Test suite() {
        def allTests = new GroovyTestSuite()
        allTests.addTestSuite(BrowserTest.class)
        return allTests
    }
}

TestRunner.run(AllTests.suite())

/**
 * Test with server
 */
class AllTestsServer{
    static Test check(){
        def tests = new GroovyTestSuite()
        //todo make test for server test
        //tests.addTestSuite(LoginTest.class)
        //tests.addTestSuite(BoardTest.class)
        //tests.addTestSuite(TimeTest.class)
        return tests
    }
}

TestRunner.run(AllTestsServer.check())


/*
println "try start jetty"
JettyServer.startJetty()
println "wait 10 sec"
sleep(10000)
println "kill jetty"
JettyServer.stopJetty()
*/