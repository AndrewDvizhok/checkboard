/**
 * Check dependancy
 */
@Grapes([
        @Grab("org.gebish:geb-core:2.3.1"),
        @Grab("org.seleniumhq.selenium:selenium-firefox-driver:3.141.59"),
        @Grab("org.seleniumhq.selenium:selenium-support:3.141.59")
])
import geb.Browser

class BrowserTest extends GroovyTestCase {
    void testCreating(){
        def browser = new Browser()
        assert browser.getClass().getName() == "geb.Browser"
    }
}
